package com.venumwolf.magicabilities.core.abilities.spells;

import com.venumwolf.magicabilities.core.helpers.Exp;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class Spell implements Listener {

    protected ItemStack spellItem;
    protected Map<UUID, Instant> coolDownMap;
    protected Map<String, SpellAction> spellActions;

    public Spell() {
        spellItem = null;
        coolDownMap = new HashMap<>();
        spellActions = new HashMap<>();
    }

    public Spell(HashMap<String, SpellAction> spellActions) {
        spellItem = null;
        coolDownMap = new HashMap<>();
        this.spellActions = spellActions;
    }

    /**
     * Kicks off the appropriate function for event action.
     * @param event The {@link PlayerInteractEvent}.
     */
    @EventHandler
    public void onSpellItemInteract(PlayerInteractEvent event) {
        if (event.getPlayer().getInventory().getItemInMainHand().isSimilar(spellItem)) {
            switch (event.getAction()) {
                case RIGHT_CLICK_AIR:
                    spellInteractRightAir(event);
                    return;
                case RIGHT_CLICK_BLOCK:
                    spellInteractRightBlock(event);
                    return;
                case LEFT_CLICK_AIR:
                    spellInteractLeftAir(event);
                    return;
                case LEFT_CLICK_BLOCK:
                    spellInteractLeftBlock(event);
                    return;
                case PHYSICAL:
                    spellInteractPhysical(event);
                    return;
                default:
                    return;
            }
        }
    }

    /**
     * Checks to ensure that the spell can be cast.
     * @param event
     */
    protected void initSpellCast(PlayerInteractEvent event, SpellAction action) {
        Player player = event.getPlayer();
        if (action.playerHasMana(player)) {
            if (coolDownMap.containsKey(player.getUniqueId())) {
                Instant lastCast = coolDownMap.get(player.getUniqueId());
                Instant currentTime = Instant.now();
                if (currentTime.isAfter(lastCast.plusMillis(action.getCoolDown(player)))) {
                    startSpell(player, action);
                } else {
                    playSpellFailSound(player);
                }
            } else {
                startSpell(player, action);
            }
        } else {
            playSpellFailSound(player);
        }
        event.setCancelled(true);
    }

    /**
     * Handles the sound effects, casts spell,
     * @param player
     */
    protected void startSpell(Player player, SpellAction action) {
        playSpellCastSound(player);
        action.spellCast(player);
        Exp.changePlayerExp(player, -action.getManaCost());
        coolDownMap.put(player.getUniqueId(), Instant.now());
    }

    /**
     * Play the spell sound effect.
     * @param player The player to play the sound too.
     */
    protected void playSpellCastSound(Player player) { }

    /**
     * Plays a sound that indicates the spell has failed.
     * @param player The player to play the sound too.
     */
    protected void playSpellFailSound(Player player) {
        player.playSound(player.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 1, 1);
    }

    protected abstract void spellInteractRightAir(PlayerInteractEvent event);

    protected abstract void spellInteractRightBlock(PlayerInteractEvent event);

    protected abstract void spellInteractLeftAir(PlayerInteractEvent event);

    protected abstract void spellInteractLeftBlock(PlayerInteractEvent event);

    protected abstract void spellInteractPhysical(PlayerInteractEvent event);

    public abstract Recipe getCraftingRecipe();

    public ItemStack getSpellItem() {
        return spellItem;
    }

    public Map<UUID, Instant> getCoolDownMap() {
        return coolDownMap;
    }

    public Map<String, SpellAction> getSpellActions() {
        return spellActions;
    }
}
