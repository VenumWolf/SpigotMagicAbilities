package com.venumwolf.magicabilities.core.abilities.spells.windspell;

import com.venumwolf.magicabilities.MagicAbilities;
import com.venumwolf.magicabilities.core.abilities.spells.Spell;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class WindSpell extends Spell {

    public WindSpell() {
        spellItem = new ItemStack(Material.FEATHER);
        ItemMeta itemMeta = spellItem.getItemMeta();
        itemMeta.setDisplayName(ChatColor.BLUE + "Wind Spell");
        itemMeta.setLore(new ArrayList<String>(){{
            add("Blow back your enemies and ride the wind!");
        }});
        itemMeta.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        spellItem.setItemMeta(itemMeta);

        spellActions.put("secondary", new WindSpellSecondaryAction(this, 1000, 1));
    }

    @Override
    protected void playSpellCastSound(Player player) {
        player.playSound(player.getLocation(), Sound.UI_TOAST_OUT, 1, 1);
    }

    @Override
    protected void spellInteractRightAir(PlayerInteractEvent event) {
        initSpellCast(event, spellActions.get("secondary"));
    }

    @Override
    protected void spellInteractRightBlock(PlayerInteractEvent event) {
        initSpellCast(event, spellActions.get("secondary"));
    }

    @Override
    protected void spellInteractLeftAir(PlayerInteractEvent event) {

    }

    @Override
    protected void spellInteractLeftBlock(PlayerInteractEvent event) {

    }

    @Override
    protected void spellInteractPhysical(PlayerInteractEvent event) {

    }

    @Override
    public Recipe getCraftingRecipe() {
        NamespacedKey itemKey = new NamespacedKey(MagicAbilities.getPlugin(), "wind_spell_feather");
        ShapedRecipe recipe = new ShapedRecipe(itemKey, spellItem);

        recipe.shape("*", "^", " ");

        recipe.setIngredient('^', Material.FEATHER);
        recipe.setIngredient('*', Material.ENDER_EYE);

        return recipe;
    }
}
