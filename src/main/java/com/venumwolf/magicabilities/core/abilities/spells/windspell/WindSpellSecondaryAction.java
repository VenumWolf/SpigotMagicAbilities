package com.venumwolf.magicabilities.core.abilities.spells.windspell;

import com.venumwolf.magicabilities.MagicAbilities;
import com.venumwolf.magicabilities.core.abilities.spells.Spell;
import com.venumwolf.magicabilities.core.abilities.spells.SpellAction;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class WindSpellSecondaryAction extends SpellAction {

    public WindSpellSecondaryAction(Spell spell, int coolDown, int manaCost) {
        super(spell, coolDown, manaCost);
    }

    @Override
    public void spellCast(Player player) {
        Vector direction = player.getLocation().getDirection();
        player.setVelocity(direction.multiply(2));

        new BukkitRunnable() {

            @Override
            public void run() {
                if (!player.isOnGround()) {
                    player.setFallDistance(-50);
                } else {
                    Bukkit.getScheduler().cancelTask(getTaskId());
                }
            }
        }.runTaskTimer(MagicAbilities.getPlugin(), 0, 0);
    }
}
