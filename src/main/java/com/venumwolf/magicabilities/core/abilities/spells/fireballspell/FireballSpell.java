package com.venumwolf.magicabilities.core.abilities.spells.fireballspell;

import com.venumwolf.magicabilities.MagicAbilities;
import com.venumwolf.magicabilities.core.abilities.spells.Spell;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public class FireballSpell extends Spell {

    /**
     * Default constructor: Sets up the spell item.
     */
    public FireballSpell() {
        spellItem = new ItemStack(Material.BLAZE_ROD);
        ItemMeta itemMeta = spellItem.getItemMeta();
        itemMeta.setDisplayName(ChatColor.BLUE + "Fireball Wand");
        itemMeta.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        spellItem.setItemMeta(itemMeta);

        spellActions.put("primary", new FireballSpellPrimaryAction(this, 500, 1));
    }

    @Override
    protected void spellInteractRightAir(PlayerInteractEvent event) {
    }

    @Override
    protected void spellInteractRightBlock(PlayerInteractEvent event) {
    }

    @Override
    protected void spellInteractLeftAir(PlayerInteractEvent event) {
        initSpellCast(event, spellActions.get("primary"));
    }

    @Override
    protected void spellInteractLeftBlock(PlayerInteractEvent event) {
        initSpellCast(event, spellActions.get("primary"));
    }

    @Override
    protected void spellInteractPhysical(PlayerInteractEvent event) {
        initSpellCast(event, spellActions.get("primary"));
    }

    @Override
    protected void playSpellCastSound(Player player) {
        player.playSound(player.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1, 1);
    }

    public Recipe getCraftingRecipe() {
        NamespacedKey itemKey = new NamespacedKey(MagicAbilities.getPlugin(), "fire_spell_wand");
        ShapedRecipe shapedRecipe = new ShapedRecipe(itemKey, spellItem);

        shapedRecipe.shape("*", "|", "|");

        shapedRecipe.setIngredient('*', Material.BLAZE_POWDER);
        shapedRecipe.setIngredient('|', Material.BLAZE_ROD);

        return shapedRecipe;
    }

    public ItemStack getSpellItem() {
        return spellItem;
    }
}