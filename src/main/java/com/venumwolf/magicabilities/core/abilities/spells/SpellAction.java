package com.venumwolf.magicabilities.core.abilities.spells;

import com.venumwolf.magicabilities.core.helpers.Exp;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public abstract class SpellAction implements Listener {

    protected Spell spell;
    protected int coolDown;
    protected int manaCost;

    public SpellAction(Spell spell, int coolDown, int manaCost) {
        this.spell = spell;
        this.coolDown = coolDown;
        this.manaCost = manaCost;
    }

    /**
     * Handles the casting of the spell.
     * @param player The player that casted the spell.
     */
    abstract public void spellCast(Player player);

    /**
     * Check if the player has enough mana to cast the spell.
     * @param player The player to check.
     * @return       True if the player has enough mana.
     */
    protected boolean playerHasMana(Player player) {
        return Exp.getPlayerExp(player) >= manaCost;
    }

    /**
     * Return the cool down for a specific player.
     * <br>
     * No special conditions by default.
     * @param player The player to check.
     * @return       The cool down time in ms for the player.
     */
    public int getCoolDown(Player player) {
        return coolDown;
    }

    public void setCoolDown(int coolDown) {
        this.coolDown = coolDown;
    }

    public int getManaCost() {
        return manaCost;
    }

    public void setManaCost(int manaCost) {
        this.manaCost = manaCost;
    }
}
