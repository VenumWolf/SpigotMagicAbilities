package com.venumwolf.magicabilities.core.abilities.spells.fireballspell;

import com.venumwolf.magicabilities.MagicAbilities;
import com.venumwolf.magicabilities.core.abilities.spells.Spell;
import com.venumwolf.magicabilities.core.abilities.spells.SpellAction;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.time.Instant;
import java.util.ArrayList;
import java.util.UUID;

public class FireballSpellPrimaryAction extends SpellAction {
    protected ArrayList<UUID> fireballList;

    public FireballSpellPrimaryAction(Spell spell, int coolDown, int manaCost) {
        super(spell, coolDown, manaCost);
        fireballList = new ArrayList<>();
    }

    @Override
    public void spellCast(Player player) {
        Fireball fireball = player.launchProjectile(SmallFireball.class);
        fireball.setIsIncendiary(true);
        fireball.setVelocity(player.getEyeLocation().getDirection().multiply(4));

        spell.getCoolDownMap().put(player.getUniqueId(), Instant.now());
        fireballList.add(fireball.getUniqueId());

        new BukkitRunnable() {
            int ticksPassed = 0;
            int ticksToLive = 5;

            /**
             * Handles the particle effects for the spell and terminates it after a set time.
             */
            @Override
            public void run() {
                if (ticksPassed > ticksToLive || fireball.isDead()) {
                    fireball.remove();
                    fireballList.remove(fireball.getUniqueId());
                    Bukkit.getScheduler().cancelTask(getTaskId());

                } else {
                    spawnFireballParticleEffect(fireball.getLocation());
                    ticksPassed++;
                }
            }
        }.runTaskTimer(MagicAbilities.getPlugin(), 0, 0);
    }

    private void spawnFireballParticleEffect(Location location) {
        location.getWorld().spawnParticle(Particle.LAVA, location, 5);
        location.getWorld().spawnParticle(Particle.SMOKE_LARGE, location.getX(), location.getY(), location.getZ(),
                10, 0.1, 0.1, 0.1, 0.1, null, false);
    }

    /**
     * Checks for and handles instances of fireball spells.
     * @param event The {@link EntityDamageByEntityEvent} to check and process.
     */
    @EventHandler
    public void onFireballSpellHitEntity(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Fireball) {
            Fireball fireball = (Fireball) event.getDamager();

            if (fireballList.contains(fireball.getUniqueId())) {
                event.setDamage(5.0);
                event.getEntity().setFireTicks(120);
            }
        }
    }
}
