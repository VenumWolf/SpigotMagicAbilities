package com.venumwolf.magicabilities.core.abilities.passvie;

import com.venumwolf.magicabilities.core.helpers.Exp;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ManaRegeneration extends BukkitRunnable {
    int maxRegenLevel = 1;

    @Override
    public void run() {
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            if (Exp.getPlayerExp(player) < Exp.getExpAtLevel(maxRegenLevel)) {
                Exp.changePlayerExp(player, 1);
            }
        }
    }
}
