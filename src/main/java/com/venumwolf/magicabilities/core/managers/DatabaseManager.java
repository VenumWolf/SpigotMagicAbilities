package com.venumwolf.magicabilities.core.managers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {
    protected String host;
    protected int port;
    protected String databaseName;
    protected String username;
    protected String password;

    protected Connection connection;

    /**
     * Default constructor.
     * @param host         The database's hostname.
     * @param port         The database's port.
     * @param databaseName The database name to connect to.
     * @param username     The username to log in with.
     * @param password     The password to log in with.
     */
    public DatabaseManager(String host, int port, String databaseName, String username, String password) {
        this.host = host;
        this.port = port;
        this.databaseName = databaseName;
        this.username = username;
        this.password = password;
    }

    public void createConnection() throws SQLException {
        if (connection != null && connection.isClosed()) {
            return;
        }

        connection = DriverManager.getConnection(getDatabaseUrl());
    }

    protected String getDatabaseUrl() {
        return "jdbc:mysql://" + host + ":" + port + "/" + databaseName + username + password;
    }
}