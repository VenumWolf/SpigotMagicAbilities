package com.venumwolf.magicabilities;

import com.venumwolf.magicabilities.core.abilities.spells.fireballspell.FireballSpell;
import com.venumwolf.magicabilities.core.abilities.passvie.ManaRegeneration;
import com.venumwolf.magicabilities.core.abilities.spells.windspell.WindSpell;
import org.bukkit.plugin.java.JavaPlugin;

public final class MagicAbilities extends JavaPlugin {
    protected static MagicAbilities plugin;

    public MagicAbilities() {
        setPlugin(this);
    }

    @Override
    public void onEnable() {
        getLogger().info("Loading Magical Abilities.");

        // Create instances of all spells.
        FireballSpell fireballSpell = new FireballSpell();
        WindSpell windSpell = new WindSpell();

        // Register events from all spells.
        getServer().getPluginManager().registerEvents(fireballSpell, this);
        getServer().getPluginManager().registerEvents(windSpell, this);

        // Add item recipes from all spells.
        getServer().addRecipe(fireballSpell.getCraftingRecipe());
        getServer().addRecipe(windSpell.getCraftingRecipe());

        // Start background tasks.
        new ManaRegeneration().runTaskTimer(this, 0, 120);

        getLogger().info("Finished loading Magical Abilities.");
    }

    @Override
    public void onDisable() { }

    protected static void setPlugin(MagicAbilities plugin) {
        MagicAbilities.plugin = plugin;
    }

    public static MagicAbilities getPlugin() {
        return plugin;
    }
}

